package org.bindiego.email;

/**
 * Created by wubin on 14-9-11.
 */
public class Settings {
    public static final String IMAP_PROVIDER = "imaps";
    public static final String SMTP_PROVIDER = "smtp";

    public static final String EMAIL_IMAP = "imap.gmail.com";
    public static final Integer EMAIL_IMAP_PORT = 993;
    public static final Boolean EMAIL_IMAP_SSL = true;

    public static final String EMAIL_SMTP = "smtp.gmail.com";
    public static final Integer EMAIL_SMTP_PORT = 587;
    public static final Boolean EMAIL_SMTP_SSL = true;

    public static final String USER = "wubin@edanzgroup.com";
    public static final String PASSWD = "";

    public static final String INBOX = "INBOX";

    public static final String MIME_MULTI = "multipart/*";
    public static final String MIME_TEXT = "text/*";

    public static final String MIME_ALT = "multipart/alternative";
    public static final String MIME_PLAIN = "text/plain";
    public static final String MIME_HTML = "text/html";

    public static final String CONF_FILE = "./conf/config.properties";
}
