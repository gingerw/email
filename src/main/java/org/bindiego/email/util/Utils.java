package org.bindiego.email.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.ConfigurationException;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.InternetAddress;
import java.io.*;
import java.util.*;

import javax.activation.DataHandler;

import org.bindiego.email.Settings;

public class Utils
{
    private static final Logger logger = 
        LogManager.getFormatterLogger(Utils.class.getName());
    private PropertiesConfiguration config;

    public Utils() {}

    public void test()
    {
        config = Config.getConfig();

        logger.info(config.getProperty("app.name").toString()
            + " testing application started");

        Properties props = new Properties();
        Session session = null;
        Store store = null;
        Folder inbox = null;

        try {
            // setup connection
            session = Session.getDefaultInstance(props, null);
            store = session.getStore(Settings.IMAP_PROVIDER);
            store.connect(Settings.EMAIL_IMAP, Settings.USER, Settings.PASSWD);

            // open inbox folder
            inbox = store.getFolder(Settings.INBOX);
            inbox.open(Folder.READ_ONLY);

            // get list of javamail messages as an array of messages
            int numOfMsg = inbox.getMessageCount();
            logger.debug("there are %d messages in the inbox", numOfMsg);

            Message[] messages = inbox.getMessages();
            for (Message msg : messages) {
                if (msg.getSubject().contains("SOAP failure from Japan SugarCRM")) {
                    logger.debug("Found you buddy");

                    Object content = msg.getContent();
                    if (content instanceof Multipart) {
                        logger.debug("Found Multipart content");

                        BodyPart clearTxt = null;
                        BodyPart htmlTxt = null;
                        String t = null;

                        Multipart parts = (Multipart)content;

                        for (int i = 0; i < parts.getCount(); ++i) {
                            BodyPart part = parts.getBodyPart(i);

                            String disposition = part.getDisposition();

                            // BodyPart.ATTACHMENT doesn't work for gmail
                            if (disposition != null &&
                                (disposition.equalsIgnoreCase("ATTACHMENT"))) {
                                DataHandler handler = part.getDataHandler();

                                logger.debug("Found attachment %s", handler.getName());
                            } else if (part.isMimeType(Settings.MIME_PLAIN)) {
                                clearTxt = part;

                                logger.debug("Found %s body.", Settings.MIME_PLAIN);
                            } else if (part.isMimeType(Settings.MIME_HTML)) {
                                htmlTxt = part;

                                logger.debug("Found %s body.", Settings.MIME_HTML);
                            } else if (part.isMimeType(Settings.MIME_MULTI)) {
                                t = getText(part);

                                logger.debug("Found %s body.", Settings.MIME_MULTI);
                            } else {
                                logger.debug("Unknown body part: %s", part);
                            }
                        }

                        if (clearTxt != null) {
                            logger.debug("Plain text body is:\n %s", clearTxt.getContent().toString());
                        } else if (htmlTxt != null) {
                            String html = htmlTxt.getContent().toString();

                            logger.debug("HTML body is:\n %s", html);
                            logger.debug("Parsed HTML body is:\n %s", parseHtml(html));
                        } else if (t != null) {
                            logger.debug("getText() returned:\n %s", t);

                            logger.debug("start print line by line");
                            String[] lines = 
                                t.split(System.getProperty("line.separator"));
                            for (String line : lines) {
                                logger.debug(line);
                            }
                            logger.debug("end print line by line");
                        }
                    } else if (content instanceof String)  { // String content object found
                        logger.debug("Found String content object: " + content.toString());
                    } else { // not a mime message
                        logger.warn("Not mime message, message to string: " + msg.toString());
                    }
                }
            }

            TreeSet<String> froms = new TreeSet<String>();

            for (int i = 0; i < messages.length; ++i) {
                String from = getFrom(messages[i]);

                if (from != null) {
                    from = removeQuotes(from);
                    froms.add(from);
                }
            }

            Iterator<String> it = froms.iterator();
            while (it.hasNext()) {
                logger.debug("from: " + it.next());
            }
        } catch (NoSuchProviderException e) {
            logger.error("", e);
        } catch (MessagingException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        } finally {
            if (inbox != null) {
                try {
                    inbox.close(false);
                } catch (MessagingException e) {
                    logger.error("", e);
                }
            }

            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    logger.error("", e);
                }
            }
        }

        logger.info("Email testing application ended");
    }

    public String parseHtml(String html) {
        String text = 
            Jsoup.parse(html.replaceAll("(?i)<br[^>]*>", "br2n")).text();
        text = text.replaceAll("br2n", "\n");
        return text;
    }

    public String getText(Part p) throws MessagingException, IOException {
        if (p.isMimeType(Settings.MIME_PLAIN)) {
            logger.debug("Found %s body.", Settings.MIME_PLAIN);
            return p.getContent().toString();
        } else if (p.isMimeType(Settings.MIME_HTML)) {
            logger.debug("Found %s body.", Settings.MIME_HTML);
            return parseHtml(p.getContent().toString());
        }

        if (p.isMimeType(Settings.MIME_ALT)) {
            // prefer html text over plain text
            Multipart mp = (Multipart)p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); ++i) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType(Settings.MIME_PLAIN)) {
                    if (text == null)
                        text = getText(bp);
                    continue;
                } else if (bp.isMimeType(Settings.MIME_HTML)) {
                    String s = getText(bp);
                    if (s != null)
                        return s;
                } else {
                    return getText(bp);
                }
            }
        } else if (p.isMimeType(Settings.MIME_MULTI)) {
            Multipart mp = (Multipart)p.getContent();
            for (int i = 0; i < mp.getCount(); ++i) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }

        return null;
    }

    public String getFrom(Message javaMailMessage)
            throws MessagingException {
        String from = "";
        Address a[] = javaMailMessage.getFrom();
        if (a == null) return null;
        for (int i=0; i < a.length; i++) {
            Address address = a[i];
            from = from + address.toString();
        }

        return from;
    }

    public String removeQuotes(String stringToModify) {
        int indexOfFind = stringToModify.indexOf(stringToModify);
        if ( indexOfFind < 0 ) return stringToModify;

        StringBuffer oldStringBuffer = new StringBuffer(stringToModify);
        StringBuffer newStringBuffer = new StringBuffer();
        for ( int i=0, length=oldStringBuffer.length(); i<length; i++ ) {
            char c = oldStringBuffer.charAt(i);
            if ( c == '"' || c == '\'' ) {
                // do nothing
            }
            else {
                newStringBuffer.append(c);
            }

        }

        return new String(newStringBuffer);
    }

    public void appendToFile(String filename, String text) {
        PrintWriter fout = null;
        try {
            fout = new PrintWriter(
                new BufferedWriter(
                    new FileWriter(filename, true)));

            fout.println(text);
        } catch (IOException e) {
            logger.error("Failed to write file %s", filename);
        } finally {
            try{
                fout.close();
            } catch (Exception e) {
                logger.error("Failed to close file %s", filename);
            }
        }
    }
}
