package org.bindiego.email;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.ConfigurationException;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;

import org.bindiego.email.util.Config;
import org.bindiego.email.util.Utils;

public class App
{
    private static final Logger logger = 
        LogManager.getFormatterLogger(App.class.getName());
    private static PropertiesConfiguration config;

    public static void main( String... args )
    {
        Utils utils = new Utils();
        utils.test();
    }
}
