package org.bindiego.email.edanz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.ConfigurationException;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.InternetAddress;
import java.io.*;
import java.util.*;

import javax.activation.DataHandler;

import org.bindiego.email.Settings;
import org.bindiego.email.util.*;

public class Edanz {
    private static final Logger logger = 
        LogManager.getFormatterLogger(Edanz.class.getName());
    private PropertiesConfiguration config;
    private Utils utils;

    private String out = "./out.txt";

    private String sep = "--------------";
    private String deli = "=>";
    private String module_name = "[module_name]";
    private String name = "[name]";
    private String value = "[value]";

    private String sub_jp = "SOAP failure from Japan SugarCRM";
    private String from_jp ="apache@r2.edanzediting.com";

    private String sub_cn = "China test Sugar SOAP failure";
    private String from_cn = "editing@liwenbianji.cn";

    private String sub_gl = "Global Sugar SOAP failure";
    private String from_gl ="apache@r2.edanzediting.com";

    private Date errorDate = new Date((2015 - 1900), 0, 28);

    private String sys_jp = "Japan";
    private String sys_cn = "China";
    private String sys_gl = "Global";
    

    public Edanz() {
        utils = new Utils();

        config = Config.getConfig();

        try {
            File file = new File(out);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            logger.error("Failed to remove file %s", out);
        }

        getErrors(errorDate);
    }

    public String parseSoapErrStr(String str) {
        String rtn = new String("");

        String[] lines = str.split(System.getProperty("line.separator"));

        for (String line : lines) {
            line = line.trim();
            if (line.trim().startsWith(module_name)) {
                rtn += line.split(deli)[1].trim();
                rtn += ", ";
            } else if (line.trim().startsWith(value)) {
                rtn += line.split(deli)[1].trim();
                rtn += ", ";
            }
        }

        rtn = rtn.trim();
        if (rtn.endsWith(",")) {
            rtn = rtn.substring(0, rtn.length() - 1);
        }

        return rtn;
    }

    public void getErrors(Date received)
    {
        StringBuilder jp = new StringBuilder();
        StringBuilder cn = new StringBuilder();
        StringBuilder gl = new StringBuilder();

        logger.info(config.getProperty("app.name").toString()
            + " Edanz getErrors started");

        logger.debug("Error Date: %s", received);

        Properties props = new Properties();
        Session session = null;
        Store store = null;
        Folder inbox = null;

        try {
            // setup connection
            session = Session.getDefaultInstance(props, null);
            store = session.getStore(Settings.IMAP_PROVIDER);
            store.connect(Settings.EMAIL_IMAP, Settings.USER, Settings.PASSWD);

            // open inbox folder
            inbox = store.getFolder(Settings.INBOX);
            inbox.open(Folder.READ_ONLY);

            // get list of javamail messages as an array of messages
            int numOfMsg = inbox.getMessageCount();
            logger.debug("there are %d messages in the inbox", numOfMsg);
            int start = 1;
            int max = 1000;
            int end = numOfMsg < max ? numOfMsg : max;

            //Message[] messages = inbox.getMessages(start, end);
            Message[] messages = inbox.getMessages();
            for (Message msg : messages) {
                /*
                logger.debug("Sub: %s\nDate received: %s\nFrom: %s", 
                    msg.getSubject(), msg.getReceivedDate(),
                    InternetAddress.toString(msg.getFrom()));
                logger.debug(msg.getReceivedDate().after(received));
                */
                if (msg.getReceivedDate().after(received)) {
                    String system = null;

                    if (msg.getSubject().contains(sub_jp)
                        && InternetAddress.toString(msg.getFrom())
                            .contains(from_jp)) {
                        system = sys_jp;
                    } else if (msg.getSubject().contains(sub_cn)
                        && InternetAddress.toString(msg.getFrom())
                            .contains(from_cn)) {
                        system = sys_cn;
                    } else if (msg.getSubject().contains(sub_gl)
                        && InternetAddress.toString(msg.getFrom())
                            .contains(from_gl)) {
                        system = sys_gl;
                    } else {
                        continue;
                    }

                    Object content = msg.getContent();
                    String plainStr = null;
                    BodyPart clearTxt = null;
                    BodyPart htmlTxt = null;
                    String t = null;

                    if (content instanceof Multipart) {
                        logger.debug("Found Multipart content");

                        Multipart parts = (Multipart)content;

                        for (int i = 0; i < parts.getCount(); ++i) {
                            BodyPart part = parts.getBodyPart(i);

                            String disposition = part.getDisposition();

                            // BodyPart.ATTACHMENT doesn't work for gmail
                            if (disposition != null &&
                                (disposition.equalsIgnoreCase("ATTACHMENT"))) {
                                DataHandler handler = part.getDataHandler();

                                logger.debug("Found attachment %s", handler.getName());
                            } else if (part.isMimeType(Settings.MIME_PLAIN)) {
                                clearTxt = part;

                                logger.debug("Found %s body.", Settings.MIME_PLAIN);
                            } else if (part.isMimeType(Settings.MIME_HTML)) {
                                htmlTxt = part;

                                logger.debug("Found %s body.", Settings.MIME_HTML);
                            } else if (part.isMimeType(Settings.MIME_MULTI)) {
                                t = utils.getText(part);

                                logger.debug("Found %s body.", Settings.MIME_MULTI);
                            } else {
                                logger.debug("Unknown body part: %s", part);
                            }
                        }

                    } else if (content instanceof String)  { // String content object found
                        plainStr = content.toString();
                        logger.debug("Found String content object: " + plainStr);
                    } else { // not a mime message
                        logger.warn("Not mime message, message to string: " + msg.toString());
                    }

                    String txt = null;
                    if (clearTxt != null) {
                        txt = clearTxt.getContent().toString();
                    } else if (htmlTxt != null) {
                        String html = htmlTxt.getContent().toString();

                        txt = utils.parseHtml(html);
                    } else if (t != null) {
                        txt = t;
                    }

                    if (txt == null && plainStr != null)
                        txt = plainStr;

                    logger.debug("One line info: %s", system + ", " 
                        + parseSoapErrStr(txt));

                    if (system.equalsIgnoreCase(sys_jp)) {
                        jp.append(system + ", " 
                            + parseSoapErrStr(txt) + "\n");
                    } else if (system.equalsIgnoreCase(sys_cn)) {
                        cn.append(system + ", " 
                            + parseSoapErrStr(txt) + "\n");
                    } else if (system.equalsIgnoreCase(sys_gl)) {
                        gl.append(system + ", " 
                            + parseSoapErrStr(txt) + "\n");
                    }
                }
            }

        } catch (NoSuchProviderException e) {
            logger.error("", e);
        } catch (MessagingException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        } finally {
            if (inbox != null) {
                try {
                    inbox.close(false);
                } catch (MessagingException e) {
                    logger.error("", e);
                }
            }

            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    logger.error("", e);
                }
            }

            utils.appendToFile(out, jp.toString());
            utils.appendToFile(out, sep);
            utils.appendToFile(out, cn.toString());
            utils.appendToFile(out, sep);
            utils.appendToFile(out, gl.toString());
        }

        logger.info("Email Edanz getErrors ended");
    }
}
